package khmer.org.config;

import java.io.File;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.ini4j.Wini;

public class PropertyHandlerConfig {

	private final Logger logger = Logger.getLogger(PropertyHandlerConfig.class.getName());
	private static PropertyHandlerConfig instance = null;
	private Properties props = null;
	private Wini ini = null;

	private PropertyHandlerConfig() {
		try {
			props = new Properties();
			InputStream in = getClass().getClassLoader().getResourceAsStream("config.properties");
			props.load(in);
			in.close();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Error when loading DAO properties file\n***\n{0}", e.getMessage());
		}
	}

	public static synchronized PropertyHandlerConfig getInstance() {
		if (instance == null) {
			instance = new PropertyHandlerConfig();
		}
		return instance;
	}

	public String getValue(String propKey) {
		return this.props.getProperty(propKey);
	}

	public String getValue(String mainString, String propKey) {
		String res = "";
		try {
			this.ini = new Wini(
					new File("E:\\THATT THONN  DATA\\WEB_API\\demo\\src\\main\\resources\\config.properties"));
			res = ini.get(mainString, propKey);
			System.out.println("Res: " + res);
		} catch (Exception e) {

		}
		return res;
	}

}
