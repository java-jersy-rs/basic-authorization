package khmer.org.config;

import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import khmer.org.customException.DataNotFoundException;

@Provider
@PreMatching
public class RequestFilterConfig implements ContainerRequestFilter {

	// Create request authentication filter
	private static final String AUTHORIZATION_HEADER_KEY = "Authorization";
	private static final String AUTHORIZATION_HEADER_PREFIX = "Basic";
	private static final String SECURED_URL_PREFIX = "api";

	public void filter(ContainerRequestContext requestContext) throws IOException {
		System.out.println("Print requested!");
		String methodOverride = requestContext.getHeaderString("X-Http-Method-Override");
		if (methodOverride != null)
			requestContext.setMethod(methodOverride);

		/*
		 * you can use this way to checked String authHeader =
		 * request.getHeaderString(HttpHeaders.AUTHORIZATION); if (authHeader == null)
		 * throw new NotAuthorizedException("Bearer"); String token =
		 * parseToken(authHeader); if (verifyToken(token) == false) { throw new
		 * NotAuthorizedException("Bearer error=\"invalid_token\""); }
		 */

		// request authentication
		System.out.println("contain api string : "
				+ requestContext.getUriInfo().getAbsolutePath().toString());
		if (requestContext.getUriInfo().getAbsolutePath().toString().contains(SECURED_URL_PREFIX)) {
			List<String> authHeader = requestContext.getHeaders().get(AUTHORIZATION_HEADER_KEY);
			if (authHeader != null && authHeader.size() > 0) {
				String authToken = authHeader.get(0);
				authToken = authToken.replaceFirst(AUTHORIZATION_HEADER_PREFIX, "").trim();
				String decodeString = new String(Base64.decode(authToken.getBytes()));
				StringTokenizer tokenizer = new StringTokenizer(decodeString, ":");
				String username = tokenizer.nextToken();
				String password = tokenizer.nextToken();

				if ("user".equals(username) && "password".equals(password)) {
					System.out.print("username and password is correct");
					return;
				}
			}
			System.out.print("username and password is incorrect");
			Response unAuthorizedStatus = Response.status(Response.Status.UNAUTHORIZED)
					.entity("User cannot access the resourece").build();
			requestContext.abortWith(unAuthorizedStatus);
		}

	}

}
