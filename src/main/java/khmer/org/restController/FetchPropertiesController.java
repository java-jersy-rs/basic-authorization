package khmer.org.restController;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import khmer.org.config.PropertyHandlerConfig;
import khmer.org.model.Person;

@Path("/resource")
@Api("/resource")
public class FetchPropertiesController {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All Person")
	@ApiResponses(value = { @ApiResponse(code = 200, response = Person.class, message = "Successful operation"),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 422, message = "Invalid data", response = Error.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
	public Response getAllPerson() {
		System.out.println("Config: " + PropertyHandlerConfig.getInstance().getValue("API_SECRET"));
		return Response.status(Response.Status.OK).entity(PropertyHandlerConfig.getInstance().getValue("API_SECRET"))
				.build();
	}

	@GET
	@Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "All Person")
	@ApiResponses(value = { @ApiResponse(code = 200, response = Person.class, message = "Successful operation"),
			@ApiResponse(code = 400, message = "Bad Request", response = Error.class),
			@ApiResponse(code = 422, message = "Invalid data", response = Error.class),
			@ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
	public Response est() {
		System.out.println("Config: " + PropertyHandlerConfig.getInstance().getValue("API_SECRET"));
		return Response.status(Response.Status.OK)
				.entity(PropertyHandlerConfig.getInstance().getValue("CHNL_GATE", "MAIL_SUB")).build();
	}
}
