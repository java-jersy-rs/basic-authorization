package khmer.org.customException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import khmer.org.model.ErrorMessage;

@Provider
public class DataNoteFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

	public Response toResponse(DataNotFoundException ex) {
		System.out.println("Error happen..DataNoteFoundExceptionMapper");
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), "404", "http://localhost:8080/");
		return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).type(MediaType.APPLICATION_JSON).build();
	}

}
