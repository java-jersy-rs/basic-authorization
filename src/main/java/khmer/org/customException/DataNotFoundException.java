package khmer.org.customException;

import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataNotFoundException(String message) {
		super(message);
		System.out.println("Error happen..DataNotFoundException");
	}

}
